/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jeffreysanti.classrapfinder;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author jeffrey
 */
public class MetaData {
    
    static void prepareDatabase(Connection c){
        
        try {
            Statement s = c.createStatement();
            s.executeUpdate("CREATE TABLE IF NOT EXISTS classmeta ("
                    + "cuid INT, "
                    + "propnane TEXT, "
                    + "propval TEXT)");
            
            
        } catch (SQLException ex) {
            Logger.getLogger(MetaData.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
