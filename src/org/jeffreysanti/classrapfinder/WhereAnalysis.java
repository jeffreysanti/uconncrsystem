/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jeffreysanti.classrapfinder;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author jeffrey
 */
public class WhereAnalysis {
    
    static public class Classroom{
        String bldingCode;
        String room;
        int capacity;
        String roomCode;
    }
    
    static void AnalyzeBldingGroupPage(String addr, ArrayList<Classroom> C){
        String doc = Util.httpRequest(addr);
        
        doc = doc.substring(doc.indexOf("<div class=\"entry-content\">"));
        doc = doc.substring(0, doc.indexOf("<footer class=\"entry-footer\">"));
        doc = doc.replaceAll("\n", ""); // remove newlines for pattern matching later
        
        // now match each table row
        Pattern p = Pattern.compile("<tr>(.*?)</tr>");
        Pattern pInnerTD = Pattern.compile("<td(.*?)>(.*?)</td>");
        Pattern pRoomName = Pattern.compile("<a(.*?)>(.*?)</a>");
        Matcher m = p.matcher(doc);
        String row = "";
        int indx = -1;
        while(m.find()) {
            indx ++;
            if(indx == 0)
                continue; // row heading
            row = m.group(1);
            
            Classroom classroom = new Classroom();
            
            // now match columns
            Matcher mRow = pInnerTD.matcher(row);
            int column = 1;
            while(mRow.find()){
                String col = mRow.group(2);
                //System.out.println(col);
                if(column == 1){ // room name
                    Matcher roomName = pRoomName.matcher(col);
                    if(roomName.find()){
                        classroom.room = roomName.group(2);
                    }else{
                        break;
                    }
                }
                if(column == 2){
                    classroom.bldingCode = col;
                }
                if(column == 3){
                    col = col.replaceAll(" ", "");
                    try{
                        classroom.capacity = Integer.parseInt(col);
                    }catch(java.lang.NumberFormatException ex){
                        classroom.capacity = -1;
                    }
                    C.add(classroom);
                    
                    // generate room code
                    String roomNo = classroom.room.substring(classroom.room.lastIndexOf(" ")+1);
                    classroom.roomCode = classroom.bldingCode+roomNo;
                    System.out.println(classroom.roomCode);
                    break;
                }
                column ++;
            }
        }
    }
    
    static ArrayList<Classroom> getAllClassrooms(String addr){
        ArrayList<Classroom> C = new ArrayList();
        String doc = Util.httpRequest(addr);
        
        // find Building List
        doc = doc.substring(doc.indexOf("<div class=\"entry-content\">"));
        doc = doc.substring(0, doc.indexOf("<footer class=\"entry-footer\">"));
        
        // now each anchor tag we should search
        Pattern p = Pattern.compile("href=\"(.*?)\"");
        Matcher m = p.matcher(doc);
        addr = "";
        while(m.find()) {
            addr = "http://classrooms.uconn.edu/" + m.group(1);
            AnalyzeBldingGroupPage(addr, C);
        }
        return C;
    }
    
    static void findClassroomsToDB(String addr, Connection c)
    {
        try {
            ArrayList<Classroom> C = getAllClassrooms(addr);
            
            Statement s = c.createStatement();
            s.executeUpdate("CREATE TABLE IF NOT EXISTS classrooms ("
                    + "code TEXT, "
                    + "name TEXT, "
                    + "capacity INT)");
            s = c.createStatement();
            s.executeUpdate("DELETE FROM classrooms");
            
            PreparedStatement ps = c.prepareStatement("INSERT INTO classrooms (code, name, capacity) VALUES"
                    + " (?,?,?)");
            
            for(Classroom cl : C){
                ps.setString(1, cl.roomCode);
                ps.setString(2, cl.room);
                ps.setInt(3, cl.capacity);
                ps.executeUpdate();
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(WhereAnalysis.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
}
