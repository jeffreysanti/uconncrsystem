/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jeffreysanti.classrapfinder;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.Arrays;
import java.util.HashSet;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.firefox.internal.ProfilesIni;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;



/**
 *
 * @author jeffrey
 */
public class WhenAnalysis {
    
    static public class TimeLocationPair{
        public TimeLocationPair(String r, int s, int e, String day){
            rm = r;
            timeStart = s;
            timeEnd = e;
            weekDay = day;
        }
        String rm;
        int timeStart;
        int timeEnd;
        String weekDay;
    }
    
    static public class Section{
        int uuid = 1;
        String catalog = "";
        String section = "";
        String name = "";
        int autouuidA = 0;
        int autouuidB = 0;
        int enrollment;
        int cuidnum;
        String tmpAutoEnroll = "";
        String instructor = "";
        ArrayList<TimeLocationPair> when = new ArrayList();
        
    }
    
    static String timeValToString(int v){
        String suffix = "AM";
        if(v >= 12*60){
            v -= 12*60;
            suffix = "PM";
        }
        int hour = v / 60;
        int min = v % 60;
        String mins = Integer.toString(min);
        if(mins.length() == 1){
            mins = mins + "0";
        }
        String hours = Integer.toString(hour);
        if(hours.equals("0"))
            hours = "12";
        return hours + ":" + mins + suffix;
    }
    
    static int getTimeVal(String time){
        time = time.trim();
        time = time.toLowerCase();
        
        String origTime = time;
        int val = 0;
        
        if(time.contains("pm")){
            if(time.contains("12:")){
                val = 0; // pm alredy taken into account by 12hrs
            }else{
                val += 12*60;
            }
        }else{
            if(time.contains("12:")){
                val -= 12*60;
            }else{
                val = 0;
            }
        }
        time = time.replace("am", "").replace("pm", "");
        try{
            val += 60 * Integer.parseInt(time.substring(0, time.indexOf(":")));
            val += Integer.parseInt(time.substring(time.indexOf(":")+1));
        }catch(NumberFormatException ex){
            System.out.println("Time Invalid: "+origTime);
            return 9999;
        }
        return val;
    }
    
    static RemoteWebDriver establishClassSearchSession(String addrInital, String addrSearch, String semester, String session){
        // establish a webbrowser
        System.out.println("Establishing PeopleSoftSession");
        //HtmlUnitDriver driver = new HtmlUnitDriver(BrowserVersion.FIREFOX_24);
        
         System.setProperty("webdriver.firefox.marionette","geckodriver");
        
        ProfilesIni profile = new ProfilesIni();
        FirefoxProfile myprofile = profile.getProfile("default");
        myprofile.setAcceptUntrustedCertificates(true);
        myprofile.setAssumeUntrustedCertificateIssuer(true);
        
        FirefoxDriver driver = new FirefoxDriver();
        //driver.setJavascriptEnabled(true);

        // login
        driver.get(addrInital);

        // switch to search page
        driver.get(addrSearch);
        driver.switchTo().frame("TargetContent");
        driver.findElement(By.linkText("Search for a Course")).click();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.switchTo().parentFrame();
        driver.switchTo().frame("TargetContent");
        driver.findElement(By.cssSelector("a.PSHYPERLINK > span")).click();

        
        System.out.println("Prepareing For Queries");
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        
        // prepare for queries
        Select dropdown = new Select(driver.findElement(By.id("UC_DERIVED_GST_STRM")));
        dropdown.selectByVisibleText(semester);
        
        try {
            System.out.println("Waiting 1 Second...");
            Thread.sleep(1000);
        } catch (InterruptedException ex) {
            Logger.getLogger(WhenAnalysis.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        if(!session.isEmpty()){
            dropdown = new Select(driver.findElement(By.id("UC_DERIVED_GST_SESSION_CODE")));
            dropdown.selectByVisibleText(session);
        }

        // storrs campus
        while(!driver.findElement(By.id("CAMPUS_TBL$selm$0$$0")).isSelected()){
            driver.findElement(By.id("CAMPUS_TBL$selm$0$$0")).click();
        }

        // open enrollment: We want all classes not just open ones
        while(driver.findElement(By.id("UC_DERIVED_GST_ENRL_STAT")).isSelected()){
            driver.findElement(By.id("UC_DERIVED_GST_ENRL_STAT")).click();
        }
        
        // TODO: Select Session here
        
        return driver;
    }
    
    static String[] DEPT_IGNORE = {
        "ALL",
        ""
    };
    
    static ArrayList<String> getDepartmentList(RemoteWebDriver driver, Connection c) throws SQLException{
        
        Arrays.sort(DEPT_IGNORE); // needed for binary searches

        // List of Departments
        
        PreparedStatement s = c.prepareStatement("SELECT * FROM classes WHERE dept=?");
        
        Select sel = new Select(driver.findElement(By.id("UC_DERIVED_GST_SUBJECT")));
        ArrayList<String> DEPT = new ArrayList();
        for(WebElement elm : sel.getOptions()){
            if(elm.getAttribute("value") == null)
                continue;
            if(elm.getAttribute("value").isEmpty())
                continue;
            
            if(Arrays.binarySearch(DEPT_IGNORE, elm.getAttribute("value")) >= 0){
                continue;
            }
            
            s.setString(1, elm.getAttribute("value"));
            ResultSet r = s.executeQuery();
            if(r.next()){
                System.out.println("Skipping Dept: "+elm.getAttribute("value"));
                continue;
            }
            
            DEPT.add(elm.getAttribute("value"));
        }
        DEPT.sort(String.CASE_INSENSITIVE_ORDER);
        return DEPT;
    }
    
    static ArrayList<Section> getDepartmentSections(RemoteWebDriver driver, String dept, int uuidCounter)
    {
        driver.switchTo().parentFrame();
        driver.switchTo().frame("TargetContent");
        
        Select dropdown = new Select(driver.findElement(By.id("UC_DERIVED_GST_SUBJECT")));
        dropdown.selectByValue(dept);
        
        System.out.println("Fetching Courses Dept:" + dept);

        driver.findElement(By.id("UC_DERIVED_GST_SEARCH_PB")).click();
        /*try {
            Thread.sleep(500);
        } catch (InterruptedException ex) {
            Logger.getLogger(WhenAnalysis.class.getName()).log(Level.SEVERE, null, ex);
        }*/
        WebDriverWait wait = new WebDriverWait(driver,30);
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("WAIT_win0")));
        
        System.out.println("  * Progress Bar Stopped");
        
        
        // check if messaged popped up, telling us it could take time
        driver.switchTo().defaultContent();
        if(driver.findElementsById("#ICOK").size() > 0){
            
            System.out.println(driver.findElementsById("#ICOK"));
            
            if(driver.getPageSource().contains("Your search did not return any result.")){
                System.out.println("  * NO RESULTS");
                driver.findElementById("#ICOK").click();
                //wait = new WebDriverWait(driver,20);
                return new ArrayList();
            }
            else if(driver.getPageSource().contains("would return over")){
                System.out.println("  * TOO MANY RESULTS");
                driver.findElementById("#ICOK").click();
                //wait = new WebDriverWait(driver,20);
                return new ArrayList();
            }
            
            System.out.println("  * Dialog Displayed, OK");
            driver.findElementById("#ICOK").click();
            wait = new WebDriverWait(driver,20);
            /*try {
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
                Logger.getLogger(WhenAnalysis.class.getName()).log(Level.SEVERE, null, ex);
            }*/
            wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("WAIT_win0")));
            System.out.println("  * Progress Bar Stopped");
        }
        
        driver.switchTo().frame("TargetContent");
        System.out.println("  * Source Receieved");

        String html = driver.getPageSource();
        html = html.substring(html.indexOf("UC_CLASS_G_VW$scroll$0"));
        html = html.substring(html.indexOf("\"PSLEVEL1GRID\""));
        html = html.substring(html.indexOf("<tbody>"));
        html = html.substring(0, html.indexOf("</tbody>"));
        html = html.substring(html.indexOf("</tr>")); // remove heading row
        html = html.substring(html.indexOf("<tr ")); // start at next <tr> tag
        
        String [] courses = html.split("<tr ");
        ArrayList<ArrayList<String>> DTA = new ArrayList();
        for(String c : courses){
            if(!c.contains("<td "))
                continue;
            ArrayList<String> ENT = new ArrayList();
            String [] fields = c.substring(c.indexOf("<td ")).split("<td ");
            //System.out.println("NEW");
            for(String f : fields){
                f = f.trim().replaceAll("\n", "");
                if(f.isEmpty())
                    continue;
                f = "<td "+f;
                String stripped = f.replaceAll("<[^>]*>", "").trim();
                if(stripped.equals(" "))
                    stripped = "";
                if(stripped.equals("  "))
                    stripped = "";
                if(stripped.equals("\t"))
                    stripped = "";
                ENT.add(stripped);
                //System.out.println("   : "+stripped);
            }
            if(ENT.size() == 18){
                DTA.add(ENT);
            }
        }

        // now for each course
        ArrayList<Section> S = new ArrayList();
        ArrayList<Section> returnAutoEnroll = new ArrayList();
        for(ArrayList<String> course : DTA){
            uuidCounter ++;
            Section s = new Section();
            s.uuid = uuidCounter;
            s.catalog = course.get(2);
            s.section = course.get(3);
            if(!course.get(1).trim().equalsIgnoreCase(dept)){
                System.out.println("Dept Mismatch: Did it load? -- " + dept + " vs "+course.get(1).trim());
                return new ArrayList();
            }
            s.name = course.get(8);
            if(!course.get(9).equals("In Person") && !course.get(9).equals("Interactive Television") &&
                    !course.get(9).equals("Hybrid/Blended")){
                System.out.println(course.get(9));
                continue;
            }
            s.enrollment = Integer.parseInt(course.get(13));
            try{
                s.cuidnum = Integer.parseInt(course.get(0));
            }catch(NumberFormatException ex){
                s.cuidnum = -1;
            }
            s.instructor = course.get(17);
            s.instructor = s.instructor.replaceAll("\n", " ");
            s.instructor = s.instructor.replaceAll("\t", " ");
            while(true){ // strip extra spaces here
                String old = s.instructor;
                s.instructor = s.instructor.replaceAll("  ", " ");
                if(old.equals(s.instructor))
                    break;
            }

            // time & location
            
            String [] timelocs = course.get(10).split("&");
            for(String timeLoc : timelocs){
                String [] seps = timeLoc.replaceAll("amp;", "").split("/");
                if(seps.length != 3){
                    System.out.println("/ DIVISON NOT 3");
                    continue;
                }
                int tStart = getTimeVal(seps[0].substring(0, seps[0].indexOf("-")));
                int tEnd = getTimeVal(seps[0].substring(seps[0].indexOf("-") +1));
                String blding = seps[2].trim();
                if(blding.isEmpty())
                    continue;
                String days = seps[1];
                if(days.contains("Mo"))
                    s.when.add(new TimeLocationPair(blding, tStart, tEnd, "Monday"));
                if(days.contains("Tu"))
                    s.when.add(new TimeLocationPair(blding, tStart, tEnd, "Tuesday"));
                if(days.contains("We"))
                    s.when.add(new TimeLocationPair(blding, tStart, tEnd, "Wednesday"));
                if(days.contains("Th"))
                    s.when.add(new TimeLocationPair(blding, tStart, tEnd, "Thursday"));
                if(days.contains("Fr"))
                    s.when.add(new TimeLocationPair(blding, tStart, tEnd, "Friday"));
            }
            if(s.when.isEmpty())
                continue;
            if(course.get(11).length() > 1){
                String stmp = course.get(10);
                stmp = stmp.replace("You will be auto-enrolled in section(s)", "").trim();
                returnAutoEnroll.add(s);
                s.tmpAutoEnroll = stmp;
            }
            S.add(s);
        }

        // now resolve autoenroll sections
        for(Section s : returnAutoEnroll){
            String sections = s.tmpAutoEnroll;
            String [] secs = sections.split(",");
            String enrollA = secs[0].trim();
            for(Section sSearch : S){
                if(sSearch.section.equals(enrollA) && sSearch.catalog.equals(s.catalog)){
                    s.autouuidA = sSearch.uuid;
                    break;
                }
            }
            if(secs.length > 1){
                String enrollB = secs[1].trim();
                for(Section sSearch : S){
                    if(sSearch.section.equals(enrollB) && sSearch.catalog.equals(s.catalog)){
                        s.autouuidB = sSearch.uuid;
                        break;
                    }
                }
            }
        }
        return S;
    }
    
    static int writeDeptSectionsToDB(ArrayList<Section> S, Connection c, String dept, int nextUUID){
        try {
            c.setAutoCommit(false);
            // clear old data
            PreparedStatement ps = c.prepareStatement("DELETE FROM classes WHERE dept=?");
            ps.setString(1, dept);
            ps.executeUpdate();
            ps.close();
            
            ps = c.prepareStatement("INSERT INTO classes (uuid, uccid, dept, catalog, sec, name, " + 
                    "enrollment, autoA, autoB, instructor, room, day, start, end) " +
                    "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
            
            ps.setString(3, dept);
            for(Section s : S){
                ps.setInt(1, s.uuid);
                ps.setInt(2, s.cuidnum);
                ps.setString(4, s.catalog);
                ps.setString(5, s.section);
                ps.setString(6, s.name);
                ps.setInt(7, s.enrollment);
                ps.setInt(8, s.autouuidA);
                ps.setInt(9, s.autouuidB);
                ps.setString(10, s.instructor);
                for(TimeLocationPair loc : s.when){
                    ps.setString(11, loc.rm);
                    ps.setString(12, loc.weekDay);
                    ps.setInt(13, loc.timeStart);
                    ps.setInt(14, loc.timeEnd);
                    ps.executeUpdate();
                }
                nextUUID = Math.max(nextUUID, s.uuid);
            }
            ps.close();
            c.setAutoCommit(true);
            
        } catch (SQLException ex) {
            Logger.getLogger(WhenAnalysis.class.getName()).log(Level.SEVERE, null, ex);
        }
        return nextUUID + 1;
    }
    
    static void prepareDB(Connection c)
    {
        try {
            Statement s = c.createStatement();
            s.executeUpdate("CREATE TABLE IF NOT EXISTS classes ("
                    + "uuid INT, "
                    + "uccid INT, "
                    + "dept TEXT, "
                    + "catalog TEXT, "
                    + "sec TEXT, "
                    + "name TEXT, "
                    + "enrollment INT, "
                    + "autoA INT, "
                    + "autoB INT, "
                    + "instructor TEXT, "
                    + "room TEXT, "
                    + "day TEXT, "
                    + "start INT, "
                    + "end INT)");
            s.close();
        } catch (SQLException ex) {
            Logger.getLogger(WhenAnalysis.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    static int nextUUID(Connection c)
    {
        try {
            Statement s = c.createStatement();
            ResultSet r = s.executeQuery("SELECT uuid FROM classes ORDER BY uuid DESC LIMIT 1");
            if(r.next()){
                int id = r.getInt("uuid") + 1;
                r.close();
                s.close();
                return id;
            }
            r.close();
            s.close();
        } catch (SQLException ex) {
            Logger.getLogger(WhenAnalysis.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 1;
    }
    
    
    
    
    static void AnalyizeWhen(String addrInital, String addrSearch, Connection c, String semester, String session, String startAt){
        
        
        prepareDB(c);
        int uuidCounter = nextUUID(c);
        
        RemoteWebDriver driver = establishClassSearchSession(addrInital, addrSearch, semester, session);
        
        ArrayList<String> DEPT = null;
        try {
            DEPT = getDepartmentList(driver, c);
        } catch (SQLException ex) {
            Logger.getLogger(WhenAnalysis.class.getName()).log(Level.SEVERE, null, ex);
        }
        boolean startNow = false;
        if(startAt.isEmpty())
            startNow = true;
        int completed = 0;
        for(String dept : DEPT){
            if(!startNow && !startAt.isEmpty()){
                if(!startAt.equals(dept)){
                    System.out.println("StartAt Skipping: "+dept);
                    completed ++;
                    continue;
                }else{
                    startNow = true;
                }
            }
            ArrayList<Section> S = null;
            S = getDepartmentSections(driver, dept, uuidCounter);
            uuidCounter = writeDeptSectionsToDB(S, c, dept, uuidCounter+1);
            completed ++;
            System.out.println(dept + ": "+S.size()+" Sections");
            System.out.println("Completed "+completed + " / "+DEPT.size());
        }
        
        
        // TOOK 90 minutes + 22 minutes
    }

}
