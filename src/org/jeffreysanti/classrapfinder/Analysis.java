/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jeffreysanti.classrapfinder;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author jeffrey
 */
public class Analysis {
    
    static void roomAvalibility(String nm, String day, Connection c){
        try {
            PreparedStatement stmt = c.prepareStatement("SELECT start,end FROM classes WHERE room LIKE ? AND day LIKE ? "+
                    " GROUP BY start,end ORDER BY start ASC");
            stmt.setString(1, nm);
            stmt.setString(2, day);
            
            ResultSet r = stmt.executeQuery();
            int timeNow = 0;
            while(r.next()){
                int timeFreeEnds = r.getInt("start");
                
                System.out.println(WhenAnalysis.timeValToString(timeNow) +
                        " - " + WhenAnalysis.timeValToString(timeFreeEnds) +
                        "   [ "+(timeFreeEnds-timeNow) + " ]");
                
                timeNow = r.getInt("end");
            }
            
            System.out.println(WhenAnalysis.timeValToString(timeNow) +
                        " - " + WhenAnalysis.timeValToString(24*60-1) +
                        "   [ "+((24*60-1)-timeNow) + " ]");
            
            
        } catch (SQLException ex) {
            Logger.getLogger(Analysis.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    static void classesThisTime(String day, int timeVal, Connection c){
        try {
            PreparedStatement stmt = c.prepareStatement("SELECT * FROM classes WHERE day LIKE ? "+
                    " AND start <= ? AND end >= ? ORDER BY enrollment DESC");
            stmt.setString(1, day);
            stmt.setInt(2, timeVal);
            stmt.setInt(3, timeVal);
            
            ResultSet r = stmt.executeQuery();
            while(r.next()){
                System.out.println(
                    r.getString("dept") + " " +
                    r.getString("catalog") + " [" +
                    r.getInt("enrollment") + "] IN: "+
                    r.getString("room"));
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(Analysis.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    static void classesCapacityAtLeast(int num, Connection c, boolean showTimes){
        try {
            PreparedStatement stmt = c.prepareStatement("SELECT dept, catalog, sec, name, enrollment, instructor, room " +
                    " FROM classes WHERE enrollment >= ? AND NOT(room = \" \")"+
                    " GROUP BY dept,catalog,sec  ORDER BY enrollment DESC");
            stmt.setInt(1, num);
            ResultSet r = stmt.executeQuery();
            while(r.next()){
                if(r.getString("room").length() < 3)
                    continue;
                System.out.println(
                    r.getString("dept") + "," +
                    r.getString("catalog") +  "," +
                    r.getString("sec") +  "," +
                    r.getInt("enrollment") + "," + r.getString("room") + ","+
                    r.getString("instructor").replaceAll("\r", " ").trim());
                
                if(showTimes){
                    PreparedStatement stmt2 = c.prepareStatement("SELECT day, start, end FROM classes WHERE dept=? AND catalog=? AND sec=?" +
                            " ORDER BY CASE WHEN day='Monday' THEN 1" +
                            "               WHEN day='Tuesday' THEN 2" +
                            "               WHEN day='Wednesday' THEN 3" +
                            "               WHEN day='Thursday' THEN 4" +
                            "               WHEN day='Friday' THEN 5 END ASC");
                    stmt2.setString(1, r.getString("dept"));
                    stmt2.setString(2, r.getString("catalog"));
                    stmt2.setString(3, r.getString("sec"));
                    ResultSet r2 = stmt2.executeQuery();
                    while(r2.next()){
                        System.out.print(",,       "+r2.getString("day")+"  ");
                        System.out.print(WhenAnalysis.timeValToString(r2.getInt("start")) + " - ");
                        System.out.println(WhenAnalysis.timeValToString(r2.getInt("end")));
                    }
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(Analysis.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    static void classesDeptNumberAtLeast(String [] dept, int minCNum, Connection c, boolean showTimes){
        try {
            PreparedStatement stmt = c.prepareStatement("SELECT * FROM classes WHERE dept LIKE ? LIMIT 1");
            for(String d : dept){
                stmt.setString(1, d);
                ResultSet r = stmt.executeQuery();
                if(!r.next()){
                    System.out.println("Warning: DEPT '"+d+"' has no courses.\n");
                }
                r.close();
            }
            stmt.close();
            String query = "SELECT dept, catalog, sec, name, enrollment, instructor, room " +
                    " FROM classes WHERE (";
            for(int i=0; i<dept.length; i++){
                if(i != 0)
                    query += " OR ";
                query += " dept LIKE ? ";
            }
            query += ") AND cast(catalog AS INTEGER) > ? GROUP BY dept,catalog,sec ORDER BY enrollment DESC";
            stmt = c.prepareStatement(query);
            for(int i=0; i<dept.length; i++){
                stmt.setString(i+1, dept[i]);
            }
            stmt.setInt(dept.length+1, minCNum);
            
            
            
            
            ResultSet r = stmt.executeQuery();
            while(r.next()){
                if(r.getString("room").length() < 3)
                    continue;
                System.out.println(
                    r.getString("dept") + "," +
                    r.getString("catalog") +  "," +
                    r.getString("sec") +  "," +
                    r.getString("name") + "," +
                    r.getInt("enrollment") + ","+
                    r.getString("instructor").replaceAll("\r", " ").trim());
                
                if(showTimes){
                    PreparedStatement stmt2 = c.prepareStatement("SELECT room, day, start, end FROM classes WHERE dept=? AND catalog=? AND sec=?" +
                            " ORDER BY CASE WHEN day='Monday' THEN 1" +
                            "               WHEN day='Tuesday' THEN 2" +
                            "               WHEN day='Wednesday' THEN 3" +
                            "               WHEN day='Thursday' THEN 4" +
                            "               WHEN day='Friday' THEN 5 END ASC");
                    stmt2.setString(1, r.getString("dept"));
                    stmt2.setString(2, r.getString("catalog"));
                    stmt2.setString(3, r.getString("sec"));
                    ResultSet r2 = stmt2.executeQuery();
                    while(r2.next()){
                        System.out.print(",        "+r2.getString("room")+","+r2.getString("day")+"  ");
                        System.out.print(WhenAnalysis.timeValToString(r2.getInt("start")) + " - ");
                        System.out.println(WhenAnalysis.timeValToString(r2.getInt("end")));
                    }
                }
            }
            
            
        } catch (SQLException ex) {
            Logger.getLogger(Analysis.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
