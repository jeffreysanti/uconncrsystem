/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jeffreysanti.classrapfinder;


import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;


/**
 *
 * @author jeffrey
 */
public class ClassRapFinder {

    static{
		System.setProperty("org.apache.commons.logging.Log", "org.apache.commons.logging.impl.SimpleLog");
		System.setProperty("org.apache.commons.logging.simplelog.log.org.apache.http", "warn");
	}
    
    public static final String CR_PAGE = "http://classrooms.uconn.edu/classrooms-alphabetical/";
    public static final String DCS_LOGIN = "https://student.studentadmin.uconn.edu/CSGUE/signon.html";
    public static final String DCS_SEARCHADDR = "https://student.studentadmin.uconn.edu/psp/CSGUE/EMPLOYEE/HRMS/s/WEBLIB_PTPP_SC.HOMEPAGE.FieldFormula.IScript_AppHP?pt_fname=CO_EMPLOYEE_SELF_SERVICE&FolderPath=PORTAL_ROOT_OBJECT.CO_EMPLOYEE_SELF_SERVICE&IsFolder=true";

    
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        Connection connection = null;
        ResultSet resultSet = null;
        Statement statement = null;
 
        try {
            //File f = new File("classes.db");
            //if(f.exists()){
            //    f.delete();
            //}
            
            Class.forName("org.sqlite.JDBC");
            connection = DriverManager.getConnection("jdbc:sqlite:fall2016.db");
            
            Util.prepareSSLSecurityHole();
            
            
            //WhereAnalysis.findClassroomsToDB(CR_PAGE, connection);
            WhenAnalysis.AnalyizeWhen(DCS_LOGIN, DCS_SEARCHADDR, connection, "Fall 2016", "Regular Academic", "");
            MetaData.prepareDatabase(connection);
            
            //Analysis.roomAvalibility("LH102", "Thursday", connection);
            //Analysis.classesThisTime("Tuesday", 690, connection); // @ 11:30 on Tues
            
            //Analysis.classesCapacityAtLeast(60, connection, true);
            /*Analysis.classesDeptNumberAtLeast(new String[]{
                        "POLS",
                        "ANTH",
                        "ENGL",
                        "SOCI",
                        "COMM",
                        "AMST",
                        "EVST",
                        "ENVE",
                        "PP",
                        "PUBH",
                        "NRE",
                        "JOUR",
                        "PHIL",
                        "HRTS"
                    }, 3000, connection, true);
            */
            
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        
    }
    
}
